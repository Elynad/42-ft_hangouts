# ft_hangouts

## Subject :

"The goal of this project is to get you acquainted with Android app
development. You will create, in JAVA, a contact management mobile app
on Google’s Android platform. You will have to understand how an Android
app functions, how Android manages your application and how to use the
SDK."

## Notes :

The project can also be made in Kotlin.

## Features :

Contacts :
----------
:heavy_check_mark: Contact creation  
:heavy_check_mark: Contact edition  
:heavy_check_mark: Contact deletion  
:heavy_check_mark: Save contacts in a SQLite Database  
:heavy_check_mark: Retrieve contacts from Database  
:heavy_check_mark: When creating a contact, set validators on mandatory fields

SMS :
-----
:heavy_check_mark: Receive SMS  
:heavy_check_mark: Send SMS  
:heavy_check_mark: Delete SMS when a contact is removed  
:heavy_check_mark: Delete SMS

Views :
-------
:heavy_check_mark: Splashscreen that checks for permissions  
:heavy_check_mark: Home view with a contacts list  
:heavy_check_mark: View to create a new contact or edit it  
:heavy_check_mark: Dialog to see contacts info  
:heavy_check_mark: View to send and receive SMS

Other :
-------
:heavy_check_mark: Change header color  
:heavy_check_mark: Support at least 2 languages
:heavy_check_mark: Display when the app has been put in background when we come back  
:heavy_check_mark: App icon & logo should be 42 logo  
:heavy_check_mark: Check permissions in every activity and go back to SplashScreen if one is missing

Bonuses :
---------
:heavy_check_mark: When receiving a SMS from an unknown contact, create it
:heavy_check_mark: Call contact
:heavy_check_mark: Send mail to contact
:heavy_check_mark: Clean architecture (MVVM)
:heavy_check_mark: Material design
:heavy_check_mark: Profile picture for contacts


Bug fixes :
-----------
:o: Check scrolling  
:heavy_check_mark: Disable send button when message is empty
:heavy_check_mark: Auto format phone numbers