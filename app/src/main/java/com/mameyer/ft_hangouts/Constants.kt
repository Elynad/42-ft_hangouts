package com.mameyer.ft_hangouts

import android.graphics.Color
import androidx.appcompat.widget.Toolbar

object Constants {

    // Intent types
    const val TAG_INFO = 0
    const val TAG_CALL = 1
    const val TAG_MAIL = 2
    const val TAG_SMS = 3
    const val TAG_CONTACT = "contact"

    // Result codes
    const val RESULT_CODE_NEW_CONTACT = 10
    const val RESULT_CODE_EDIT_CONTACT = 11


    // Colors
    const val COLOR_BLACK = 91
    const val COLOR_YELLOW = 92
    const val COLOR_CYAN = 93
    const val COLOR_GREEN = 94
    const val COLOR_RED = 95

    /**
     *  Handle the [Toolbar] color changes.
     *  @param toolbar [Toolbar] (Nullable) - The [Toolbar] we want to change color.
     *  @param color [Int] - An [Int] corresponding to the color the user picked.
     */
    fun changeHeaderColor(toolbar: Toolbar?, color : Int) : Boolean {
        Singletons.color =
            when (color) {
                COLOR_BLACK -> Color.BLACK
                COLOR_YELLOW -> Color.YELLOW
                COLOR_CYAN -> Color.CYAN
                COLOR_GREEN -> Color.GREEN
                COLOR_RED -> Color.RED
                else -> Color.BLACK
            }
        toolbar?.setBackgroundColor(Singletons.color)
        return (true)
    }
}