package com.mameyer.ft_hangouts

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.mameyer.ft_hangouts.model.Contact
import com.mameyer.ft_hangouts.model.CustomSms
import java.lang.Exception

// TODO : Surround every call of database with a try / catch
class DatabaseHandler(context: Context) : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(SQL_CREATE_CONTACTS_TABLE)
        db?.execSQL(SQL_CREATE_MESSAGES_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun addContact(contact: Contact, success: (Contact) -> Unit, failure: () -> Unit) {
        try {
            val db = this.writableDatabase
            val values = ContentValues()
            values.put(TAG_FIRST_NAME, contact.firstName)
            values.put(TAG_LAST_NAME, contact.lastName)
            values.put(TAG_PHONE, contact.phoneNumber)
            values.put(TAG_BIRTHDAY, contact.birthDay)
            values.put(TAG_EMAIL, contact.email)
            values.put(TAG_COMPANY, contact.company)
            values.put(TAG_PICTURE, contact.picture)
            val result = db.insert(CONTACT_TABLE_NAME, null, values)
            db.close()
            Log.v("Inserted ID", "$result")
            if (result > 0) {
                contact.id = result
                success.invoke(contact)
            } else failure.invoke()
        } catch (exception : Exception) {
            Log.e(TAG, "Failed to add contact : ${exception.message}")
            failure.invoke()
        }
    }

    fun editContact(contact : Contact, success: () -> Unit, failure: () -> Unit) {
        try {
            val db = this.writableDatabase
            val values = ContentValues()
            values.put(TAG_FIRST_NAME, contact.firstName)
            values.put(TAG_LAST_NAME, contact.lastName)
            values.put(TAG_PHONE, contact.phoneNumber)
            values.put(TAG_BIRTHDAY, contact.birthDay)
            values.put(TAG_EMAIL, contact.email)
            values.put(TAG_COMPANY, contact.company)
            values.put(TAG_PICTURE, contact.picture)
            val result = db.update(CONTACT_TABLE_NAME, values, "ID is ${contact.id}", null)
            if (result >= 1) {
                success.invoke()
            } else failure.invoke()
        } catch (exception : Exception) {
            Log.e(TAG, "Failed to edit contact : ${exception.message}")
            failure.invoke()
        }
    }

    fun deleteContact(contact: Contact, success: () -> Unit, failure: () -> Unit) {
        try {
            val db = this.writableDatabase
            val result = db.delete(CONTACT_TABLE_NAME, "ID is ${contact.id}", null)
            db.close()
            if (result >= 1) {
                success.invoke()
            } else failure.invoke()
        } catch (exception : Exception) {
            Log.e(TAG, "Failed to delete contact : ${exception.message}")
            failure.invoke()
        }
    }

    fun getAllContacts(success : (ArrayList<Contact>) -> Unit, failure : () -> Unit) {
        try {
            val result = ArrayList<Contact>()
            val db = readableDatabase
            val cursor = db.rawQuery(SQL_GET_CONTACTS, null)
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        result.add(Contact().apply {
                            this.id = cursor.getLong(cursor.getColumnIndex(TAG_ID))
                            this.firstName = cursor.getString(cursor.getColumnIndex(TAG_FIRST_NAME))
                            this.lastName = cursor.getString(cursor.getColumnIndex(TAG_LAST_NAME))
                            this.phoneNumber = cursor.getString(cursor.getColumnIndex(TAG_PHONE))
                            this.birthDay = cursor.getString(cursor.getColumnIndex(TAG_BIRTHDAY))
                            this.email = cursor.getString(cursor.getColumnIndex(TAG_EMAIL))
                            this.company = cursor.getString(cursor.getColumnIndex(TAG_COMPANY))
                            this.picture = cursor.getString(cursor.getColumnIndex(TAG_PICTURE))
                        })
                    } while (cursor.moveToNext())
                }
            } else failure.invoke()
            cursor.close()
            db.close()
            success.invoke(result)
        } catch (exception : Exception) {
            Log.e(TAG, "Failed to get contacts : ${exception.message}")
            failure.invoke()
        }
    }

    fun getContactByNumber(phoneNumber : String, success: (Contact) -> Unit, failure: () -> Unit) {
        try {
            val result = Contact()
            val db = readableDatabase
            val rawQuery = "$SQL_GET_CONTACTS WHERE $TAG_PHONE = '$phoneNumber'"
            val cursor = db.rawQuery(rawQuery, null)
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    result.id = cursor.getLong(cursor.getColumnIndex(TAG_ID))
                    result.firstName = cursor.getString(cursor.getColumnIndex(TAG_FIRST_NAME))
                    result.lastName = cursor.getString(cursor.getColumnIndex(TAG_LAST_NAME))
                    result.phoneNumber = cursor.getString(cursor.getColumnIndex(TAG_PHONE))
                    result.birthDay = cursor.getString(cursor.getColumnIndex(TAG_BIRTHDAY))
                    result.email = cursor.getString(cursor.getColumnIndex(TAG_EMAIL))
                    result.company = cursor.getString(cursor.getColumnIndex(TAG_COMPANY))
                    result.picture = cursor.getString(cursor.getColumnIndex(TAG_PICTURE))
                } while (cursor.moveToNext())
            } else {
                failure.invoke()
                cursor.close()
                db.close()
                return
            }
            cursor.close()
            db.close()
            if (result.id != 0L) success.invoke(result)
            else failure.invoke()
        } catch (exception : Exception) {
            Log.e(TAG, "Failed to get contact with this number : ${exception.message}")
            failure.invoke()
        }
    }

    fun getMessages(contact: Contact, success: (ArrayList<CustomSms>) -> Unit, failure: () -> Unit) {
        try {
            val result = ArrayList<CustomSms>()
            val db = this.readableDatabase
            val rawQuery = SQL_GET_MESSAGES + " WHERE $TAG_PHONE = '${contact.phoneNumber}'"
            val cursor = db.rawQuery(rawQuery, null)
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        result.add(CustomSms().apply {
                            this.id = cursor.getLong(cursor.getColumnIndex(TAG_ID))
                            this.phoneNumber = cursor.getString(cursor.getColumnIndex(TAG_PHONE))
                            this.isSent = cursor.getInt(cursor.getColumnIndex(TAG_SENT))
                            this.createdAt = cursor.getString(cursor.getColumnIndex(TAG_CREATED_AT))
                            this.content = cursor.getString(cursor.getColumnIndex(TAG_MESSAGE))
                        })
                    } while (cursor.moveToNext())
                }
            }
            cursor.close()
            db.close()
            if (result.isNotEmpty()) success.invoke(result)
            else failure.invoke()
        } catch (exception : Exception) {
            Log.e(TAG, "Failed to get messages : ${exception.message}")
            failure.invoke()
        }
    }

    fun saveMessage(message : CustomSms) : Long {
        return try {
            val db = this.writableDatabase
            val values = ContentValues()
            values.put(TAG_PHONE, message.phoneNumber)
            values.put(TAG_MESSAGE, message.content)
            values.put(TAG_SENT, message.isSent)
            val result = db.insert(MESSAGES_TABLE_NAME, null, values)
            db.close()
            Log.v("Inserted ID", "$result")
            (result)
        } catch (exception : Exception) {
            Log.e(TAG, "Error while saving message : ${exception.message}")
            (-1)
        }
    }

    fun removeMessage(toRemoveId : Long) {
        try {
            val db = this.writableDatabase
            val cursor = db.rawQuery(
                "DELETE FROM $MESSAGES_TABLE_NAME WHERE $TAG_ID is $toRemoveId",
                null
            )
            Log.v(TAG, "Deleted ${cursor.count} message(s)")
            cursor.close()
        } catch (exception : Exception) {
            Log.e(TAG, "Error while removing message $toRemoveId : ${exception.message}")
        }
    }

    fun deleteAllMessagesFromThisContact(phoneNumber: String, success: () -> Unit, failure: () -> Unit) {
        try {
            val db = this.writableDatabase
            val cursor = db.rawQuery(
                "DELETE FROM $MESSAGES_TABLE_NAME WHERE $TAG_PHONE is \'$phoneNumber\'",
                null
            )
            Log.v(TAG, "Deleted ${cursor.count} message(s)")
            cursor.close()
            success.invoke()
        } catch (exception : Exception) {
            Log.e(TAG, "Error while removing contacts messages : ${exception.message}")
            failure.invoke()
        }
    }

    companion object {

        private const val TAG = "DatabaseHandler"

        private const val DB_NAME = "ft_hangouts-contacts"  // TODO : Rename with ft_hangouts-database
        private const val DB_VERSION = 1

        // SQL table names
        private const val CONTACT_TABLE_NAME = "contacts"
        private const val MESSAGES_TABLE_NAME = "messages"

        // SQL variables
        private const val TAG_ID = "id"
        private const val TAG_FIRST_NAME = "first_name"
        private const val TAG_LAST_NAME = "last_name"
        private const val TAG_PHONE = "phone_number"
        private const val TAG_BIRTHDAY = "birthday"
        private const val TAG_EMAIL = "email"
        private const val TAG_COMPANY = "company"
        private const val TAG_PICTURE = "picture"
        private const val TAG_MESSAGE = "message"
        private const val TAG_SENT = "is_sent"
        private const val TAG_CREATED_AT = "created_at"

        // SQL requests
        private const val SQL_CREATE_CONTACTS_TABLE = "CREATE TABLE IF NOT EXISTS $CONTACT_TABLE_NAME ($TAG_ID Integer PRIMARY KEY, $TAG_FIRST_NAME TEXT, $TAG_LAST_NAME TEXT, $TAG_PHONE TEXT, $TAG_BIRTHDAY DATE, $TAG_EMAIL TEXT, $TAG_COMPANY TEXT, $TAG_PICTURE TEXT)"
        private const val SQL_GET_CONTACTS = "SELECT * FROM $CONTACT_TABLE_NAME"
        private const val SQL_CREATE_MESSAGES_TABLE = "CREATE TABLE IF NOT EXISTS $MESSAGES_TABLE_NAME ($TAG_ID Integer PRIMARY KEY, $TAG_PHONE TEXT, $TAG_MESSAGE TEXT, $TAG_SENT Integer, $TAG_CREATED_AT DATETIME DEFAULT CURRENT_TIMESTAMP)"
        private const val SQL_GET_MESSAGES = "SELECT * FROM $MESSAGES_TABLE_NAME"
    }
}