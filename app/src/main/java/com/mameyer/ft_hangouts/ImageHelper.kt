package com.mameyer.ft_hangouts

import android.content.Context
import android.graphics.*
import android.graphics.Bitmap.Config
import android.graphics.PorterDuff.Mode
import android.media.MediaScannerConnection
import android.net.Uri
import android.util.Log
import androidx.core.net.toUri
import java.io.*
import java.util.*


class ImageHelper {

    /**
     *  Using canvas, this function will round the provided [Bitmap], given the [pixels].
     *  @param bitmap [Bitmap] to round
     *  @param pixels [Int] - Defines the round ratio.
     *  @return (Nullable) [Bitmap] - Rounded [Bitmap]
     */
    fun getRoundedCornerBitmap(bitmap: Bitmap, pixels: Int): Bitmap? {
        val roundedBitmap = Bitmap.createBitmap(
            bitmap.width, bitmap
                .height, Config.ARGB_8888
        )
        val canvas = Canvas(roundedBitmap)
        val color = -0xbdbdbe
        val paint = Paint()
        val rect = Rect(0, 0, bitmap.width, bitmap.height)
        val rectF = RectF(rect)
        val roundPx = pixels.toFloat()
        paint.isAntiAlias = true
        canvas.drawARGB(0, 0, 0, 0)
        paint.color = color
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint)
        paint.xfermode = PorterDuffXfermode(Mode.SRC_IN)
        canvas.drawBitmap(bitmap, rect, rect, paint)
        return roundedBitmap
    }

    /**
     *  Convert the provided [InputStream] into a (Nullable) [ByteArray].
     *  @param inputStream [InputStream] to convert
     *  @return converted [InputStream] into a [ByteArray].
     */
    fun getBytes(inputStream: InputStream): ByteArray? {
        val byteBuffer = ByteArrayOutputStream()
        val bufferSize = 1024
        val buffer = ByteArray(bufferSize)
        var len: Int
        while (inputStream.read(buffer).also { len = it } != -1) {
            byteBuffer.write(buffer, 0, len)
        }
        return byteBuffer.toByteArray()
    }

    /**
     *  When coming from the camera intent, this function will save the picture the user took.
     *  We compress the image in a JPEG format, and save it with the time in millis as name, inside
     *  the images directory (for the app).
     *  @param context [Context] of the call
     *  @param bitmapToSave (Nullable) [Bitmap] to save
     *  @return (Nullable) [Uri] of the saved image.
     */
    fun saveImage(context: Context, bitmapToSave: Bitmap): Uri? {
        val bytes = ByteArrayOutputStream()
        bitmapToSave.compress(Bitmap.CompressFormat.JPEG, 90, bytes)

        val picturesDirectory = File("${context.filesDir}$IMAGE_DIRECTORY")
        if (!picturesDirectory.exists()) {
            // If the picture directory does not exists, we create it.
            picturesDirectory.mkdirs()
        }
        try {
            val file = File(
                picturesDirectory,
                Calendar.getInstance().timeInMillis.toString() + ".jpg"
            )
            file.createNewFile()
            val fileOutputStream = FileOutputStream(file)
            fileOutputStream.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(context, arrayOf(file.path), arrayOf("image/jpeg"),
                null)
            fileOutputStream.close()
            Log.d(TAG, "File saved with path ${file.absolutePath}")
            return file.toUri()
        } catch (exception: IOException) {
            Log.e(TAG, "Failed to save image : ${exception.message}")
        }
        return null
    }

    companion object {
        private const val TAG = "ImageHelper"

        private const val IMAGE_DIRECTORY = "ft_hangouts-contact_pictures"
    }
}