package com.mameyer.ft_hangouts

import android.graphics.Color

object Singletons {

    // This variable is used to keep the current color of the toolbar.
    var color = Color.RED
}