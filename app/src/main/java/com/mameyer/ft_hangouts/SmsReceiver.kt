package com.mameyer.ft_hangouts

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.telephony.SmsMessage

@Suppress("UNCHECKED_CAST")
class SmsReceiver : BroadcastReceiver() {

    // Helper classes
    private var listener : Listener? = null

    private var bundle : Bundle? = null
    private var currentSms : SmsMessage? = null

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action == TAG_SMS_RECEIVED) {
            bundle = intent.extras
            if (bundle != null && bundle?.get("pdus") != null) {
                val protocolDescriptionUnitObject = bundle?.get("pdus") as Array<Any>
                protocolDescriptionUnitObject.forEach {
                    currentSms = getIncomingMessage(it as ByteArray)
                    if (listener != null) {
                        listener?.onMessageReceived(
                            currentSms?.displayMessageBody.toString(),
                            currentSms?.displayOriginatingAddress.toString()
                        )
                    }
                }
            }
            this.abortBroadcast
        }
    }

    private fun getIncomingMessage(protocolDescriptionUnit : ByteArray) : SmsMessage {
        val format = bundle?.getString(TAG_FORMAT)
        val result : SmsMessage = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            SmsMessage.createFromPdu(protocolDescriptionUnit, format)
        } else {
            SmsMessage.createFromPdu(protocolDescriptionUnit)
        }
        return (result)
    }

    fun setListener(listener : Listener) { this.listener = listener ; println("LISTENER SHOULD BE SET") }

    interface Listener {
        fun onMessageReceived(content : String, sender : String)
    }

    companion object {
        private const val TAG_SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED"
        private const val TAG_FORMAT = "format"
    }
}