package com.mameyer.ft_hangouts

class Validators {

    fun isEmailValid(email: String) : Boolean {
        return (EMAIL_REGEX.toRegex().matches(email))
    }

    companion object {
        private const val EMAIL_REGEX = "^[A-Za-z](.*)([@]{1})(.{1,})(\\.)(.{1,})"
    }
}