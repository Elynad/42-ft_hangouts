package com.mameyer.ft_hangouts.activity_chat

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.telephony.PhoneNumberUtils
import android.telephony.SmsManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mameyer.ft_hangouts.Constants.COLOR_BLACK
import com.mameyer.ft_hangouts.Constants.COLOR_CYAN
import com.mameyer.ft_hangouts.Constants.COLOR_GREEN
import com.mameyer.ft_hangouts.Constants.COLOR_RED
import com.mameyer.ft_hangouts.Constants.COLOR_YELLOW
import com.mameyer.ft_hangouts.Constants.changeHeaderColor
import com.mameyer.ft_hangouts.DatabaseHandler
import com.mameyer.ft_hangouts.R
import com.mameyer.ft_hangouts.Singletons
import com.mameyer.ft_hangouts.SmsReceiver
import com.mameyer.ft_hangouts.model.Contact
import com.mameyer.ft_hangouts.model.CustomSms
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class ChatActivity : AppCompatActivity() {

    // Helper classes
    private var databaseHandler : DatabaseHandler? = null
    private var smsReceiver : SmsReceiver? = null
    private var toolbar : Toolbar? = null
    private var chatAdapter = ChatAdapter {
        confirmDeleteRequest(it)
    }

    // Layout variables
    private var messageRv : RecyclerView? = null
    private var plainNewMessage : EditText? = null
    private var buttonSend : ImageView? = null
    private var title : TextView? = null

    private var contact = Contact()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        databaseHandler = DatabaseHandler(this)

        if (intent.extras != null && intent.extras?.getSerializable(TAG_CONTACT) != null) {
            contact = intent.extras?.getSerializable(TAG_CONTACT) as Contact
            retrieveMessages(contact)
        }

        // Init layout variables
        messageRv = findViewById(R.id.messages_rv)
        plainNewMessage = findViewById(R.id.plain_new_message)
        buttonSend = findViewById(R.id.button_send)
        title = findViewById(R.id.title)
        toolbar = findViewById(R.id.toolbar)

        // Setup RecyclerView
        messageRv?.layoutManager = LinearLayoutManager(this)
        messageRv?.adapter = chatAdapter

        // Setup toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.title = ""
        toolbar?.setBackgroundColor(Singletons.color)

        // Check permissions
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_DENIED
            || ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.SEND_SMS,
                Manifest.permission.RECEIVE_SMS), PERMISSION_REQUEST_SMS)
        }

        // Setup input listener
        val context = this
        plainNewMessage?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.isNullOrBlank()) {
                    ImageViewCompat.setImageTintList(
                        buttonSend!!,
                        ColorStateList.valueOf(
                            ContextCompat.getColor(context, R.color.button_disabled)
                        )
                    )
                } else {
                    ImageViewCompat.setImageTintList(
                        buttonSend!!,
                        ColorStateList.valueOf(
                            ContextCompat.getColor(context, R.color.button_enabled)
                        )
                    )
                }
                buttonSend?.isEnabled = !(s.isNullOrBlank())
            }

            override fun afterTextChanged(s: Editable?) {}

        })

        // Setup send button
        ImageViewCompat.setImageTintList(
            buttonSend!!,
            ColorStateList.valueOf(ContextCompat.getColor(this, R.color.button_disabled))
        )
        buttonSend?.isEnabled = !plainNewMessage?.text?.toString().isNullOrBlank()
        buttonSend?.setOnClickListener { sendMessage() }
        setTitle()

        // Setup receiver to catch new messages from this contact
        smsReceiver = SmsReceiver()
        smsReceiver?.setListener(object : SmsReceiver.Listener {
            override fun onMessageReceived(content: String, sender: String) {
                var formattedPhoneNumber = PhoneNumberUtils.formatNumberToE164(
                    sender,
                    Locale.getDefault().country
                )
                if (formattedPhoneNumber == null)
                    formattedPhoneNumber = PhoneNumberUtils.formatNumberToE164(
                        sender,
                        "FR"
                    )
                if (sender == contact.phoneNumber || (formattedPhoneNumber != null
                            && formattedPhoneNumber == contact.phoneNumber))
                    displayMessage(content, sender, false)
            }
        })
    }

    /**
     *  Send a new message, containing the text in the [plainNewMessage] [EditText].
     *  If the app does not have the permissions to send a SMS, [ActivityCompat.requestPermissions]
     *  is called.
     *  Otherwise, the new message is sent, and a [CustomSms] item is added inside the database and
     *  the list.
     *  Finally, we clear the [plainNewMessage] [EditText].
     */
    private fun sendMessage() {
        val messageContent = plainNewMessage?.text.toString()
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED
            && ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED) {
            val smsManager = SmsManager.getDefault() as SmsManager
            smsManager.sendTextMessage(contact.phoneNumber, null, messageContent,
                null, null)
            databaseHandler?.saveMessage(CustomSms().apply {
                this.phoneNumber = contact.phoneNumber
                this.isSent = 1
                this.content = messageContent
            })
            displayMessage(messageContent, contact.phoneNumber, true)
            plainNewMessage?.text?.clear()
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.SEND_SMS,
                Manifest.permission.RECEIVE_SMS), PERMISSION_REQUEST_SMS)
        }
    }

    /**
     *  This function will call the database to get all the previous [CustomSms] messages in this
     *  conversation.
     *  @param contact [Contact] - The [Contact] the user is chatting with in this conversation.
     */
    private fun retrieveMessages(contact: Contact) {
        databaseHandler?.getMessages(contact, {
            messageRv = findViewById(R.id.messages_rv) // Need to set again RV. Why ? Dunno.
            messageRv?.post {
                chatAdapter.addItems(it)
                messageRv?.layoutManager?.scrollToPosition(chatAdapter.itemCount - 1)
            }
        }, {
            Log.v("ERROR", "NO MESSAGES")
            Toast.makeText(this, getString(R.string.no_message), Toast.LENGTH_SHORT).show()
        })
    }

    /**
     *  This function will format the message received or sent into a [CustomSms] class, and add it
     *  into the [ChatAdapter].
     *  We always set the [sender] variable to the current contact chat, as it will be easier to
     *  retrieve all the messages with this phone number.
     *  @param content [String] - The message content
     *  @param sender [String] - The contact that sent the message, or the one which will receive it
     *  @param isSent [Boolean] - Handle whether the message was sent or received.
     */
    private fun displayMessage(content: String, sender: String, isSent: Boolean) {
        chatAdapter.insertNewMessage(CustomSms().apply {
            this.content = content
            this.isSent = if (isSent) 1 else 0
            this.phoneNumber = sender
            this.createdAt = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
            else Calendar.getInstance().time.toString()
        })
        messageRv?.scrollToPosition(chatAdapter.itemCount - 1)
    }

    /**
     *  Displays an [AlertDialog] to ask confirmation to the user if he really wants to delete this
     *  [CustomSms].
     *  The [AlertDialog.BUTTON_POSITIVE] will call the database to remove the message, then notify
     *  the [ChatAdapter] to update its content, and finally dismiss the [AlertDialog].
     *  The [AlertDialog.BUTTON_NEGATIVE] will call the [ChatAdapter] to un-select the [CustomSms],
     *  then dismiss the [AlertDialog].
     *  @param toDelete [CustomSms] - The [CustomSms] to delete.
     */
    private fun confirmDeleteRequest(toDelete : CustomSms) {
        var dialog : AlertDialog? = null

        dialog = AlertDialog.Builder(this).apply {
            setTitle(getString(R.string.delete_confirmation_title))
            setMessage(getString(R.string.delete_confirmation_message))
            setPositiveButton(getString(R.string.button_delete)) { _, _ ->
                databaseHandler?.removeMessage(toDelete.id)
                chatAdapter.removeMessage(toDelete)
                dialog?.dismiss()
            }
            setNegativeButton(getString(R.string.cancel)) { _, _ ->
                chatAdapter.unSelectItem(toDelete)
                dialog?.dismiss()
            }
        }.create()
        dialog?.show()
    }

    /**
     *  Setup the conversation title.
     */
    private fun setTitle() {
        var titleContent = getString(R.string.conversation_title)
        titleContent += when {
            contact.firstName.isNotBlank() -> {
                " ${contact.firstName}"
            }
            contact.lastName.isNotBlank() -> {
                " ${contact.lastName}"
            }
            else -> " ${contact.phoneNumber}"
        }
        title?.text = titleContent
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(smsReceiver, IntentFilter().apply {
            addAction("android.provider.Telephony.SMS_RECEIVED")
        })
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(smsReceiver)
    }

    /**
     *  Handle the permissions result, whether the user accepted the permissions or not. We saved
     *  the lastSelectedContact in order to call the right function as soon as the user accepts the
     *  permissions.
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQUEST_SMS) {
            if (grantResults.contains(-1))
                Toast.makeText(this,
                    getString(R.string.sms_permission_needed), Toast.LENGTH_LONG).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        println("ON CREATE OPTIONS MENU")
        menuInflater.inflate(R.menu.menu, menu)
        return (true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.menu_black -> { changeHeaderColor(toolbar, COLOR_BLACK) }
        R.id.menu_yellow -> { changeHeaderColor(toolbar, COLOR_YELLOW) }
        R.id.menu_cyan -> { changeHeaderColor(toolbar, COLOR_CYAN) }
        R.id.menu_green -> { changeHeaderColor(toolbar, COLOR_GREEN) }
        R.id.menu_red -> { changeHeaderColor(toolbar, COLOR_RED) }
        else -> { super.onOptionsItemSelected(item) }
    }

    companion object {
        fun getStartIntent(context: Context, contact: Contact) : Intent {
            val intent = Intent(context, ChatActivity::class.java)
            intent.putExtra(TAG_CONTACT, contact)
            return (intent)
        }
        private const val TAG_CONTACT = "contact"
        private const val PERMISSION_REQUEST_SMS = 8
    }
}
