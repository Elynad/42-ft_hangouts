package com.mameyer.ft_hangouts.activity_chat

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.mameyer.ft_hangouts.R
import com.mameyer.ft_hangouts.activity_contacts.ContactsListAdapter
import com.mameyer.ft_hangouts.model.Contact
import com.mameyer.ft_hangouts.model.CustomSms

class ChatAdapter(val delete : (CustomSms) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var messages = ArrayList<CustomSms>()

    /**
     *  Update the [ChatAdapter] with the new [CustomSms]s list.
     *  @param items [ArrayList]<[CustomSms]> - List of new [CustomSms]s items to add in the list.
     */
    fun addItems(items : ArrayList<CustomSms>) {
        this.messages = items
        notifyDataSetChanged()
    }

    fun insertNewMessage(item : CustomSms) {
        messages.add(messages.size, item)
        notifyItemInserted(messages.indexOf(item))
    }

    /**
     *  Remove a [CustomSms] from the list.
     *  @param item [CustomSms] - The [CustomSms] to remove.
     */
    fun removeMessage(item : CustomSms) {
        notifyItemRemoved(messages.indexOf(item))
        messages.remove(item)
    }

    /**
     *  Un-select a [CustomSms] from the list.
     *  @param item [CustomSms] - The [CustomSms] to un-select.
     */
    fun unSelectItem(item : CustomSms) {
        notifyItemChanged(messages.indexOf(item))
    }

    override fun getItemViewType(position: Int): Int {
        return if (messages[position].isSent == 1) TYPE_SENT else TYPE_RECEIVED
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == TYPE_SENT) MessageSentViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_message_sent, parent, false))
        else MessageReceivedViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_message_received, parent, false))
    }

    override fun getItemCount(): Int = messages.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == TYPE_SENT)
            (holder as? MessageSentViewHolder)?.setData(messages[position])
        else (holder as? MessageReceivedViewHolder)?.setData(messages[position])
    }

    /**
     *  This class handles the [View] for the sent messages.
     */
    inner class MessageSentViewHolder(view : View) : RecyclerView.ViewHolder(view) {

        private var container : CardView? = view.findViewById(R.id.container)
        private var messageContent : TextView? = view.findViewById(R.id.message_content)
        private var date : TextView? = view.findViewById(R.id.date)

        fun setData(message : CustomSms) {
            container?.setCardBackgroundColor(ContextCompat.getColor(itemView.context, R.color.colorPrimary))
            messageContent?.text = message.content
            date?.visibility = View.GONE
            date?.text = message.createdAt
            container?.setOnClickListener {
                if (date?.visibility == View.GONE) date?.visibility = View.VISIBLE
                else date?.visibility = View.GONE
            }
            container?.setOnLongClickListener {
                container?.setCardBackgroundColor(Color.RED)
                delete.invoke(message)
                true
            }
        }
    }

    /**
     *  This class handles the [View] for the received messages.
     */
    inner class MessageReceivedViewHolder(view : View) : RecyclerView.ViewHolder(view) {

        private var container : CardView? = view.findViewById(R.id.container)
        private var messageContent : TextView? = view.findViewById(R.id.message_content)
        private var date : TextView? = view.findViewById(R.id.date)

        fun setData(message: CustomSms) {
            container?.setCardBackgroundColor(ContextCompat.getColor(itemView.context, R.color.received_message))
            messageContent?.text = message.content
            date?.visibility = View.GONE
            date?.text = message.createdAt
            container?.setOnClickListener {
                if (date?.visibility == View.GONE) date?.visibility = View.VISIBLE
                else date?.visibility = View.GONE
            }
            container?.setOnLongClickListener {
                container?.setCardBackgroundColor(Color.RED)
                delete.invoke(message)
                true
            }
        }
    }

    companion object {
        private const val TYPE_SENT = 1
        private const val TYPE_RECEIVED = 0
    }
}