package com.mameyer.ft_hangouts.activity_contacts

import android.Manifest
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.telephony.PhoneNumberUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.mameyer.ft_hangouts.Constants.COLOR_BLACK
import com.mameyer.ft_hangouts.Constants.COLOR_CYAN
import com.mameyer.ft_hangouts.Constants.COLOR_GREEN
import com.mameyer.ft_hangouts.Constants.COLOR_RED
import com.mameyer.ft_hangouts.Constants.COLOR_YELLOW
import com.mameyer.ft_hangouts.Constants.RESULT_CODE_EDIT_CONTACT
import com.mameyer.ft_hangouts.Constants.RESULT_CODE_NEW_CONTACT
import com.mameyer.ft_hangouts.Constants.TAG_CALL
import com.mameyer.ft_hangouts.Constants.TAG_CONTACT
import com.mameyer.ft_hangouts.Constants.TAG_INFO
import com.mameyer.ft_hangouts.Constants.TAG_MAIL
import com.mameyer.ft_hangouts.Constants.TAG_SMS
import com.mameyer.ft_hangouts.Constants.changeHeaderColor
import com.mameyer.ft_hangouts.DatabaseHandler
import com.mameyer.ft_hangouts.R
import com.mameyer.ft_hangouts.Singletons
import com.mameyer.ft_hangouts.SmsReceiver
import com.mameyer.ft_hangouts.activity_chat.ChatActivity
import com.mameyer.ft_hangouts.activity_new_contact.NewContactActivity
import com.mameyer.ft_hangouts.model.Contact
import com.mameyer.ft_hangouts.model.CustomSms
import kotlinx.android.synthetic.main.dialog_show_contact.view.*
import java.lang.Exception
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList

class ContactsActivity : AppCompatActivity() {

    // Helper classes
    private var databaseHandler : DatabaseHandler? = null
    private var smsReceiver : SmsReceiver? = null
    private var contactsAdapter = ContactsListAdapter { contact, intent ->
        handleContactClick(contact, intent)
    }

    // Layout variables
    private var content : ConstraintLayout? = null
    private var missingPermissionsLayout : LinearLayout? = null
    private var contactsRv : RecyclerView? = null
    private var addContactButton : FloatingActionButton? = null
    private var progressCircular : ProgressBar? = null
    private var noContacts : TextView? = null
    private var toolbar : Toolbar? = null

    private var contacts = ArrayList<Contact>()
    private var lastSelectedContact : Contact? = null
    private var shouldShowSavedTime = false
    private var backgroundTime = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contacts)

        // Init database
        databaseHandler = DatabaseHandler(this)

        // Init layout variables
        content = findViewById(R.id.content)
        missingPermissionsLayout = findViewById(R.id.missing_permissions)
        contactsRv = findViewById(R.id.contacts_rv)
        addContactButton = findViewById(R.id.add_contact_button)
        progressCircular = findViewById(R.id.progress_circular)
        noContacts = findViewById(R.id.no_contact)
        toolbar = findViewById(R.id.toolbar)

        handleUiDisplay(View.GONE, View.VISIBLE, View.GONE, View.GONE)

        // Setup FloatingActionButton
        addContactButton?.setOnClickListener { addNewContact() }

        // Setup RecyclerView
        contactsRv?.adapter = contactsAdapter
        contactsRv?.layoutManager = LinearLayoutManager(this)

        // Setup toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.title = ""
        toolbar?.setBackgroundColor(Singletons.color)

        // Checks permissions
        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_DENIED
                && ContextCompat.checkSelfPermission(this,
                        Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_DENIED) {
            showPermissionsMissing()
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.SEND_SMS,
                    Manifest.permission.RECEIVE_SMS), PERMISSION_REQUEST_SMS)
        } else {
            prepareSmsAndGetContacts()
        }

        shouldShowSavedTime = true
    }

    /**
     *  This function call setup the [SmsReceiver] and registers it, then call [getContacts].
     *  Before registering the [SmsReceiver], we try to unregister it (to avoid duplicates messages)
     */
    private fun prepareSmsAndGetContacts() {
        handleUiDisplay(View.GONE, View.VISIBLE, View.GONE, View.GONE)
        if (smsReceiver == null)
            smsReceiver = SmsReceiver()
        smsReceiver?.setListener(object : SmsReceiver.Listener {
            override fun onMessageReceived(content: String, sender: String) {
                checkIfContactIsKnown(sender,
                    {
                        databaseHandler?.saveMessage(CustomSms().apply {
                            this.content = content
                            this.isSent = 0
                            this.phoneNumber = it.phoneNumber
                        })
                    }, {
                        // TODO : Show error toast (?)
                    }
                )
            }
        })
        try {
            unregisterReceiver(smsReceiver)
        } catch (exception : Exception) {
            Log.e(TAG, "smsReceiver was not registered : ${exception.message}")
        }
        registerReceiver(smsReceiver, IntentFilter().apply {
            addAction("android.provider.Telephony.SMS_RECEIVED")
        })
        getContacts()
    }

    /**
     *  This function try to find the [sender] in the database. If no one is found, it creates a new
     *  [Contact] with his phone number, save it into the database, and add it into the
     *  [ContactsListAdapter].
     *  @param sender [String] - Phone number of the sender
     *  @param success [Unit] - Function to trigger in case of success
     *  @param failure [Unit] - Function to trigger in case of error
     */
    private fun checkIfContactIsKnown(sender : String,
                                      success: (Contact) -> Unit,
                                      failure: () -> Unit) {
        var formattedPhoneNumber = PhoneNumberUtils.formatNumberToE164(
            sender,
            Locale.getDefault().country
        )
        if (formattedPhoneNumber == null)
            formattedPhoneNumber = PhoneNumberUtils.formatNumberToE164(
                sender,
                "FR"
            )
        if (formattedPhoneNumber == null)
            formattedPhoneNumber = sender
        databaseHandler?.getContactByNumber(formattedPhoneNumber,
            {
                Log.i(TAG, "Contact is known")
                success.invoke(it)
            }, {
                Log.i(TAG, "Contact is not known")
                val newContact = Contact().apply {
                    this.phoneNumber = formattedPhoneNumber
                }
                databaseHandler?.addContact(newContact,
                    {
                        Log.i(TAG, "Contact successfully saved with id ${it.id}")
                        contactsAdapter.addNewContact(it)
                        contactsRv?.scrollToPosition(0)
                        handleUiDisplay(View.VISIBLE, View.GONE, View.GONE, View.GONE)
                        success.invoke(it)
                    }, {
                        failure.invoke()
                    }
                )
            }
        )
    }

    /**
     *  Gets a list of contacts from the SQLite database, then posts the [Contact]s list in the
     *  RecyclerView.
     */
    private fun getContacts() {
        databaseHandler?.getAllContacts({
            Log.d(TAG, "Got all contacts ! Size ${it.size}")
            if (it.size == 0)
                handleUiDisplay(View.GONE, View.GONE, View.VISIBLE, View.GONE)
            else {
                this.contacts = it
                contactsRv?.post { contactsAdapter.addItems(contacts) }
                handleUiDisplay(View.VISIBLE, View.GONE, View.GONE, View.GONE)
            }
        }, {
            handleUiDisplay(View.GONE, View.GONE, View.VISIBLE, View.GONE)
        })
    }

    /**
     *  Start the [NewContactActivity] and register its result code to handle its response.
     */
    private fun addNewContact() {
        shouldShowSavedTime = false
        startActivityForResult(NewContactActivity
            .getStartIntent(this, null), REQUEST_CODE_NEW_CONTACT)
    }

    /**
     *  When the user clicks on a [Contact], this function will check which button was clicked, in
     *  order to call the right function to handle the users purpose.
     *  @param contact [Contact] - The contact clicked
     *  @param intent [Int] - Contains the type of the [Intent]
     */
    private fun handleContactClick(contact : Contact, intent : Int) {
        when (intent) {
            TAG_INFO -> openContactInfo(contact)
            TAG_CALL -> callContact(contact)
            TAG_MAIL -> sendMail(contact)
            TAG_SMS -> openChat(contact)
        }
    }

    /**
     *  Displays an [AlertDialog] with all the [Contact] info. The [AlertDialog] is dismissible,
     *  and owns 2 clickable [TextView]s used as buttons, which allow the user to edit or remove the
     *  [Contact].
     *  When an un-required information is missing on the contact, the layout that contains it is
     *  hidden.
     *  @param contact [Contact] - The contact we want to show information.
     */
    private fun openContactInfo(contact : Contact) {
        val contactDialogView = LayoutInflater.from(this)
            .inflate(R.layout.dialog_show_contact, null)
        val alertDialogBuilder = AlertDialog.Builder(this)
            .setView(contactDialogView)
        contactDialogView.first_name.text = contact.firstName
        contactDialogView.last_name.text = contact.lastName
        contactDialogView.phone_number.text = contact.phoneNumber
        contactDialogView.mail.text = contact.email
        contactDialogView.company.text = contact.company
        Log.e("ContactsActivity", "Picture = ${contact.picture}")
        if (contact.picture.isNotBlank()) {
            contactDialogView.button_add_picture.colorFilter = null
            contactDialogView.button_add_picture.setImageURI(Uri.parse(contact.picture))
        } else {
            contactDialogView.button_add_picture.setColorFilter(
                ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_IN)
            contactDialogView.button_add_picture.setImageResource(R.drawable.profile)
        }
        if (contact.firstName.isBlank()) contactDialogView.first_name.visibility = View.GONE
        if (contact.lastName.isBlank()) contactDialogView.last_name.visibility = View.GONE
        if (contact.email.isBlank()) contactDialogView.mail_layout.visibility = View.GONE
        if (contact.company.isBlank()) contactDialogView.company_layout.visibility = View.GONE
        val alertDialog = alertDialogBuilder.show()
        contactDialogView.button_edit.setOnClickListener {
            shouldShowSavedTime = false
            startActivityForResult(NewContactActivity
                .getStartIntent(this, contact), REQUEST_CODE_EDIT_CONTACT)
            alertDialog.dismiss()
        }
        contactDialogView.button_delete.setOnClickListener {
            deleteContact(contact)
            alertDialog.dismiss()
        }
    }

    /**
     *  This function handles the deletion of a [Contact] from the database. It calls
     *  [DatabaseHandler] and handle its response. If an error occurred, a [Toast] is displayed.
     *  If everything went fine, the [ContactsListAdapter] is called to remove the [Contact] from
     *  the [RecyclerView].
     *  The contact is also removed from the [Contact]s list.
     *  @param contact [Contact] - The [Contact] the user wants to delete.
     */
    private fun deleteContact(contact: Contact) {
        databaseHandler?.deleteAllMessagesFromThisContact(contact.phoneNumber, {
            databaseHandler?.deleteContact(contact, {
                contactsAdapter.removeItem(contact)
                contacts.remove(contact)
                if (contacts.size == 0)
                    handleUiDisplay(View.GONE, View.GONE, View.VISIBLE, View.GONE)
            }, {
                Toast.makeText(this, getString(R.string.error_failed_to_delete), Toast.LENGTH_LONG)
                    .show()
            })
        }, {
            Toast.makeText(this, getString(R.string.error_failed_to_delete), Toast.LENGTH_LONG)
                .show()
        })
    }

    /**
     *  This functions checks if the user authorized the app to use phone call. If he did, the call
     *  [Intent] is immediately called.
     *  If he did not, we ask for the user authorization and we stock the selected contact in
     *  [lastSelectedContact] to call it as soon as the user accept the phone calls.
     *  @param contact [Contact] (Nullable) - The [Contact] the user wants to call.
     */
    private fun callContact(contact : Contact?) {
        if (contact == null) return
        Log.d(TAG, "callContact")
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            shouldShowSavedTime = false
            Log.d(TAG, "Should call contact")
            startActivity(Intent(Intent.ACTION_CALL, Uri.parse("tel:${contact.phoneNumber}")))
        } else {
            Log.d(TAG, "No permissions to call, should request them")
            this.lastSelectedContact = contact
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CALL_PHONE),
                PERMISSION_REQUEST_CALL)
        }
    }

    /**
     *  Opens the chat by calling starting [ChatActivity] [Intent].
     *  First, this function will check the permissions to check if the user allowed the app to send
     *  sms. If it did not, the [Contact] is stored into [lastSelectedContact] to launch the
     *  [ChatActivity] as soon as the user authorize the app.
     *  @param contact [Contact] - The [Contact] the user wants to chat with.
     */
    private fun openChat(contact : Contact?) {
        if (contact == null) {
            prepareSmsAndGetContacts()
            return
        }
        shouldShowSavedTime = false
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED
            && ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED) {
            startActivity(ChatActivity.getStartIntent(this, contact))
        } else {
            this.lastSelectedContact = contact
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.SEND_SMS,
                Manifest.permission.RECEIVE_SMS), PERMISSION_REQUEST_SMS)
        }
    }

    /**
     *  Starts the mail intent.
     *  @param contact [Contact] - The [Contact] the user wants to send a mail to
     */
    private fun sendMail(contact : Contact) {   // TODO : Need extra check
        if (contact.email.isBlank()) return
        shouldShowSavedTime = false
        val mailIntent = Intent(Intent.ACTION_SENDTO)
        mailIntent.data = Uri.parse("mailto:")
        mailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(contact.email))
        mailIntent.putExtra(Intent.EXTRA_SUBJECT, "")
        startActivity(mailIntent)
    }

    /**
     *  Display the [missingPermissionsLayout] to ask to the user he needs to authorize the app.
     */
    private fun showPermissionsMissing() {
        missingPermissionsLayout?.setOnClickListener {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.SEND_SMS,
                    Manifest.permission.RECEIVE_SMS), PERMISSION_REQUEST_SMS)
        }
        handleUiDisplay(View.GONE, View.GONE, View.GONE, View.VISIBLE)
    }

    /**
     *  On Android 11 (SDK 30, code R), we cannot ask for permissions more than once. If the user
     *  deny them once, the popup to request permissions will not open again. This is why we need
     *  to bring the user to Apps section in the device Settings.
     *  This function shows a [Toast] if the user is below [Build.VERSION_CODES.R], or shows an
     *  [AlertDialog] to go to Apps Settings.
     *  @param requestCode [Int] - The request code corresponding to which feature the user wanted
     *  to use.
     */
    private fun handlePermissionDenied(requestCode: Int) {
        var strId = 0
        if (requestCode == PERMISSION_REQUEST_CALL) strId = R.string.call_permission_needed
        else if (requestCode == PERMISSION_REQUEST_SMS) strId = R.string.sms_permission_needed

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            AlertDialog.Builder(this)
                    .setTitle(R.string.permissions_needed)
                    .setMessage(strId)
                    .setPositiveButton(R.string.go_to_settings) { _, _ ->
                        val settingsIntent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", packageName, null))
                        settingsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivityForResult(settingsIntent, REQUEST_CODE_SETTINGS)
                    }
                    .setNegativeButton(R.string.cancel) { _, _ ->
                        if (requestCode == PERMISSION_REQUEST_SMS) showPermissionsMissing()
                    }
                    .create()
                    .show()
        } else Toast.makeText(this, getString(strId), Toast.LENGTH_LONG).show()
    }

    /**
     *  Handles the visibility of each layout on this [AppCompatActivity].
     *  @param contactsVisibility [Int] - Visibility of the contacts list layout
     *  @param loadingVisibility [Int] - Visibility of the loading layout
     *  @param noContactVisibility [Int] - Visibility of the "No contacts !" layout
     *  @param noPermissionVisibility [Int] - Visibility of the permissions missing layout
     */
    private fun handleUiDisplay(contactsVisibility: Int, loadingVisibility: Int,
                                noContactVisibility: Int, noPermissionVisibility: Int) {
        contactsRv?.visibility = contactsVisibility
        progressCircular?.visibility = loadingVisibility
        noContacts?.visibility = noContactVisibility
        missingPermissionsLayout?.visibility = noPermissionVisibility
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(TAG, "onActivityResult ; requestCode = $requestCode")
        if (requestCode == REQUEST_CODE_NEW_CONTACT) {
            if (resultCode == RESULT_CODE_NEW_CONTACT) {
                Toast.makeText(this, getString(R.string.contact_saved), Toast.LENGTH_SHORT)
                    .show()
                if (data != null && data.extras != null && data.extras?.get(TAG_CONTACT) != null) {
                    val newContact = data.extras?.get(TAG_CONTACT) as Contact
                    contactsAdapter.addNewContact(newContact)
                }
            }
        } else if (requestCode == REQUEST_CODE_EDIT_CONTACT) {
            if (resultCode == RESULT_CODE_EDIT_CONTACT) {
                if (data != null && data.extras != null && data.extras?.get(TAG_CONTACT) != null) {
                    val modifiedContact = data.extras?.get(TAG_CONTACT) as Contact
                    contactsAdapter.editContact(modifiedContact)
                }
            }
        } else if (requestCode == REQUEST_CODE_SETTINGS) {
            if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(this,
                            Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "PERMISSIONS GRANTED")
                prepareSmsAndGetContacts()
            } else {
                Log.d(TAG, "PERMISSIONS DENIED")
            }
        }
    }

    /**
     *  Handle the permissions result, whether the user accepted the permissions or not. We saved
     *  the [lastSelectedContact] in order to call the right function as soon as the user accepts
     *  the permissions.
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when {
            grantResults.contains(PackageManager.PERMISSION_DENIED) -> handlePermissionDenied(requestCode)
            requestCode == PERMISSION_REQUEST_CALL -> callContact(lastSelectedContact)
            requestCode == PERMISSION_REQUEST_SMS -> openChat(lastSelectedContact)
        }
    }

    /**
     *  Saves the current time to display it when the app will be resumed.
     */
    override fun onPause() {
        super.onPause()
        backgroundTime = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm"))
        else Calendar.getInstance().time.toString()
    }

    /**
     *  Displays the time saved in [backgroundTime], only if [shouldShowSavedTime] is set to true.
     */
    override fun onResume() {
        super.onResume()
        toolbar?.setBackgroundColor(Singletons.color)
        prepareSmsAndGetContacts()
        if (shouldShowSavedTime && backgroundTime.isNotBlank())
            Toast.makeText(this, "Saved at $backgroundTime", Toast.LENGTH_SHORT).show()
        else shouldShowSavedTime = true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return (true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
            R.id.menu_black -> { changeHeaderColor(toolbar, COLOR_BLACK) }
            R.id.menu_yellow -> { changeHeaderColor(toolbar, COLOR_YELLOW) }
            R.id.menu_cyan -> { changeHeaderColor(toolbar, COLOR_CYAN) }
            R.id.menu_green -> { changeHeaderColor(toolbar, COLOR_GREEN) }
            R.id.menu_red -> { changeHeaderColor(toolbar, COLOR_RED) }
            else -> { super.onOptionsItemSelected(item) }
        }

    companion object {
        private const val TAG = "ContactsActivity"

        // Request codes
        private const val REQUEST_CODE_EDIT_CONTACT = 3
        private const val REQUEST_CODE_NEW_CONTACT = 4
        private const val REQUEST_CODE_SETTINGS = 5

        // Permission requests codes
        private const val PERMISSION_REQUEST_CALL = 20
        private const val PERMISSION_REQUEST_SMS = 21
    }
}