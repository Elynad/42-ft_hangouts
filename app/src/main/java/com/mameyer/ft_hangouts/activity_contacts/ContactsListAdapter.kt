package com.mameyer.ft_hangouts.activity_contacts

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.PorterDuff
import android.net.Uri
import android.os.ParcelFileDescriptor
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.mameyer.ft_hangouts.Constants.TAG_CALL
import com.mameyer.ft_hangouts.Constants.TAG_INFO
import com.mameyer.ft_hangouts.Constants.TAG_MAIL
import com.mameyer.ft_hangouts.Constants.TAG_SMS
import com.mameyer.ft_hangouts.ImageHelper
import com.mameyer.ft_hangouts.R
import com.mameyer.ft_hangouts.model.Contact
import java.io.File
import java.io.FileDescriptor
import java.io.InputStream


class ContactsListAdapter(val selected: (Contact, Int) -> Unit)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var contacts = ArrayList<Contact>()

    /**
     *  Update the [ContactsListAdapter] with the new [Contact]s list.
     *  @param newItems [ArrayList]<[Contact]> - List of new [Contact]s items to add in the list.
     */
    fun addItems(newItems: ArrayList<Contact>) {
        this.contacts = newItems
        notifyDataSetChanged()
    }

    /**
     *  Remove a [Contact] from the list.
     *  @param toRemove [Contact] - The [Contact] to remove.
     */
    fun removeItem(toRemove: Contact) {
        notifyItemRemoved(contacts.indexOf(toRemove))
        contacts.remove(toRemove)
    }

    /**
     *  Add a new [Contact] in the list.
     *  @param toAdd [Contact] - The new [Contact] to add.
     */
    fun addNewContact(toAdd: Contact) {
        contacts.add(0, toAdd)
        notifyItemInserted(0)
    }

    /**
     *  Update a [Contact] with new information.
     *  @param toEdit [Contact] - The [Contact] to edit.
     */
    fun editContact(toEdit: Contact) {
        var index = 0
        while (index < contacts.size) {
            if (contacts[index].id == toEdit.id) {
                contacts[index] = toEdit
                break
            }
            index++
        }
        notifyItemChanged(index)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ContactViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_contact,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int = contacts.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as? ContactViewHolder)?.setData(contacts[position])
    }

    inner class ContactViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val container: CardView? = view.findViewById(R.id.container)
        private val buttonCall : ImageView? = view.findViewById(R.id.button_call)
        private val buttonMessage : ImageView? = view.findViewById(R.id.button_message)
        private val buttonMail : ImageView? = view.findViewById(R.id.button_mail)
        private val firstName: TextView? = view.findViewById(R.id.first_name)
        private val profilePicture : ImageView? = view.findViewById(R.id.profile_picture)

        /**
         *  Set the data of the [Contact] inside the [View].
         *  @param contact [Contact] - The [Contact] from the list we want to inflate the layout
         *  with.
         */
        fun setData(contact: Contact) {
            val contactDisplayName =
                if (contact.firstName.isBlank() && contact.lastName.isBlank()) contact.phoneNumber
                else "${contact.firstName} ${contact.lastName}"

            // Enabling / Disabling buttons
            buttonCall?.alpha = if (contact.phoneNumber.isBlank()) 0.4f else 1f
            buttonMessage?.alpha = if (contact.phoneNumber.isBlank()) 0.4f else 1f
            buttonMail?.alpha = if (contact.email.isBlank()) 0.4f else 1f

            // Setting onClickListeners
            container?.setOnClickListener { selected.invoke(contact, TAG_INFO) }
            buttonCall?.setOnClickListener { selected.invoke(contact, TAG_CALL) }
            buttonMessage?.setOnClickListener { selected.invoke(contact, TAG_SMS) }
            buttonMail?.setOnClickListener { selected.invoke(contact, TAG_MAIL) }

            // Setting contact data
            firstName?.text = contactDisplayName
            if (contact.picture.isBlank()) {
                profilePicture?.setImageResource(R.drawable.profile)
                profilePicture?.setColorFilter(ContextCompat.getColor(itemView.context,
                    R.color.white), PorterDuff.Mode.SRC_IN)
            } else {
                try {
                    profilePicture?.colorFilter = null
                    val imageHelper = ImageHelper()
                    val inputStream : InputStream? = itemView.context.contentResolver
                        .openInputStream(Uri.parse(contact.picture))
                    val byteArray : ByteArray? = imageHelper.getBytes(inputStream!!)
                    val options = BitmapFactory.Options()
                    val bitmap : Bitmap? = BitmapFactory
                        .decodeByteArray(byteArray, 0, byteArray!!.size, options)
                    profilePicture?.setImageBitmap(ImageHelper().getRoundedCornerBitmap(
                        bitmap!!, 200)
                    )
                } catch (exception : Exception) {
                    Log.e(TAG, "Error while setting image : ${exception.message}")
                }
            }
        }
    }

    companion object {
        private const val TAG = "ContactsListAdapter"
    }
}