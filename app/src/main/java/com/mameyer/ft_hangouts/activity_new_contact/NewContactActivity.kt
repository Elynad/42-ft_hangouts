package com.mameyer.ft_hangouts.activity_new_contact

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.telephony.PhoneNumberUtils
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker
import com.mameyer.ft_hangouts.*
import com.mameyer.ft_hangouts.Constants.COLOR_BLACK
import com.mameyer.ft_hangouts.Constants.COLOR_CYAN
import com.mameyer.ft_hangouts.Constants.COLOR_GREEN
import com.mameyer.ft_hangouts.Constants.COLOR_RED
import com.mameyer.ft_hangouts.Constants.COLOR_YELLOW
import com.mameyer.ft_hangouts.Constants.RESULT_CODE_EDIT_CONTACT
import com.mameyer.ft_hangouts.Constants.RESULT_CODE_NEW_CONTACT
import com.mameyer.ft_hangouts.Constants.changeHeaderColor
import com.mameyer.ft_hangouts.model.Contact
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*

class NewContactActivity : AppCompatActivity() {

    // Layout variables
    private var toolbar                 : Toolbar? = null
    private var buttonCancel            : ImageView? = null
    private var buttonSave              : ImageView? = null
    private var buttonAddPicture        : ImageView? = null
    private var title                   : TextView? = null
    private var plainFirstName          : TextView? = null
    private var plainLastName           : TextView? = null
    private var plainPhoneNumber        : TextView? = null
    private var plainMail               : TextView? = null
    private var plainCompany            : TextView? = null

    private var contact : Contact? = null
    private var imageUri : Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_contact)

        // Init layout variables
        toolbar = findViewById(R.id.toolbar)
        buttonCancel = findViewById(R.id.button_cancel)
        buttonSave = findViewById(R.id.button_save)
        buttonAddPicture = findViewById(R.id.button_add_picture)
        title = findViewById(R.id.title)
        plainFirstName = findViewById(R.id.plain_first_name)
        plainLastName = findViewById(R.id.plain_last_name)
        plainPhoneNumber = findViewById(R.id.plain_phone_number)
        plainMail = findViewById(R.id.plain_mail)
        plainCompany = findViewById(R.id.plain_company)

        // Setup toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.title = ""
        toolbar?.setBackgroundColor(Singletons.color)

        // Setup buttons
        buttonCancel?.setOnClickListener { this.finish() }
        buttonSave?.setOnClickListener { saveContact() }
        buttonAddPicture?.setOnClickListener { addPicture() }

        presetFields()
    }

    /**
     *  If a [Contact] is given in this activity parameters, this activity is used to edit the
     *  [Contact], rather than create a new one. For this purpose, the title is changed, and the
     *  fields are preset with the current values of the [Contact].
     *  If no [Contact] class is given, we set the activity title to new contact.
     */
    private fun presetFields() {
        if (intent.extras?.get(TAG_CONTACT) == null) {
            title?.text = getString(R.string.title_new_contact)
            buttonAddPicture?.setColorFilter(ContextCompat.getColor(this, R.color.white),
                PorterDuff.Mode.SRC_IN)
        } else {
            contact = (intent.extras?.getSerializable(TAG_CONTACT)) as Contact
            title?.text = getString(R.string.title_edit_contact)
            plainFirstName?.text = contact?.firstName
            plainLastName?.text = contact?.lastName
            plainPhoneNumber?.text = contact?.phoneNumber
            plainMail?.text = contact?.email
            plainCompany?.text = contact?.company
            if (contact?.picture?.isNotBlank()!!) {
                try {
                    buttonAddPicture?.colorFilter = null
                    buttonAddPicture?.setImageURI(Uri.parse(contact?.picture))
                    imageUri = Uri.parse(contact?.picture)
                } catch (exception : Exception) {
                    Log.e(TAG, "Failed to load image : ${exception.message}")
                }
            } else
                buttonAddPicture?.setColorFilter(ContextCompat.getColor(this, R.color.white),
                    PorterDuff.Mode.SRC_IN)
            // TODO : Birthday
        }
    }

    /**
     *  This function checks if the required fields are set. If they do not, calls the error
     *  function [showRequiredFields].
     *  If the required fields are filled, create a new instance of [Contact], set its variables and
     *  save it into the database.
     *  If the activity was started for editing a [Contact] ([contact] variable not null, we call
     *  the [editContact] function, otherwise we call [createContact].
     *  Note that the email is checked with [Validators.isEmailValid], and the phoneNumber is
     *  always formatted to E164 format before saving it in the database.
     */
    private fun saveContact() {
        Log.d(TAG, "Should save contact")
        if ((plainFirstName?.text.isNullOrBlank() && plainLastName?.text.isNullOrBlank())
            || plainPhoneNumber?.text.isNullOrBlank()) {
            return showRequiredFields()
        }
        if (plainPhoneNumber?.text?.isNotBlank()!! && plainPhoneNumber?.text?.get(0) != '+') {
            return showPhoneFormatError()
        }
        if (plainMail?.text?.isNotBlank()!! && !(Validators().isEmailValid(plainMail?.text?.toString()!!))) {
            return badFormattedEmailAddress()
        }
        var formattedPhoneNumber = PhoneNumberUtils.formatNumberToE164(
            plainPhoneNumber?.text.toString(),
            Locale.getDefault().country
        )
        if (formattedPhoneNumber == null)
            formattedPhoneNumber = PhoneNumberUtils.formatNumberToE164(
                plainPhoneNumber?.text.toString(),
                "FR"
            )
        if (formattedPhoneNumber == null)
            formattedPhoneNumber = plainPhoneNumber?.text.toString()
        val newContact = Contact().apply {
            firstName = plainFirstName?.text.toString()
            lastName = plainLastName?.text.toString()
            phoneNumber = formattedPhoneNumber
            // TODO : BirthDay
            email = plainMail?.text.toString()
            company = plainCompany?.text.toString()
            if (imageUri != null) picture = imageUri.toString()
        }
        if (contact != null) editContact(newContact)
        else createContact(newContact)
    }

    /**
     *  This function is called if we are editing a [Contact]. It will call the [DatabaseHandler]
     *  function to edit a contact, and if the call succeeds, we [finish] the [AppCompatActivity]
     *  with a [RESULT_CODE_EDIT_CONTACT] result code.
     *  @param newContact [Contact] - The [Contact] to edit, containing updated values.
     */
    private fun editContact(newContact : Contact) {
        val databaseHandler = DatabaseHandler(this)
        newContact.id = contact?.id!!
        databaseHandler.editContact(newContact, {
            val intent = Intent().apply { putExtra(TAG_CONTACT, newContact) }
            setResult(RESULT_CODE_EDIT_CONTACT, intent)
            finish()
        }, {
            Toast.makeText(this, getString(R.string.error_failed_to_edit), Toast.LENGTH_LONG)
                .show()
        })
    }

    /**
     *  This function calls [DatabaseHandler] to insert a new [Contact] in the database. We keep the
     *  inserted ID to set it into [newContact], as we want to return it to ContactsActivity. This
     *  is useful as we want to notify the contact adapter that a new [Contact] has been inserted.
     *  @param newContact [Contact] - The new [Contact] to insert in the database.
     */
    private fun createContact(newContact: Contact) {
        val databaseHandler = DatabaseHandler(this)
        databaseHandler.addContact(
            newContact,
            {
                val intent = Intent().apply { putExtra(TAG_CONTACT, it) }
                setResult(RESULT_CODE_NEW_CONTACT, intent)
                finish()
            }, {
                // TODO : Error toast
            }
        )
    }

    /**
     *  Triggered when the user clicks on the addPicture button.
     *  Will display an [AlertDialog] to ask the user if he wants to use the Gallery or the Camera,
     *  and will call [chooseImageFromGallery] or [takePictureFromCamera].
     */
    private fun addPicture() {
        val pictureDialog = AlertDialog.Builder(this)
        pictureDialog.setTitle(getString(R.string.choose_method))
        val pictureDialogItems = arrayOf(
            getString(R.string.pick_from_gallery),
            getString(R.string.capture_from_camera)
        )
        pictureDialog.setItems(pictureDialogItems) { _, which ->
            when (which) {
                0 -> chooseImageFromGallery()
                1 -> takePictureFromCamera()
            }
        }
        pictureDialog.show()
    }

    /**
     *  Checks if the app has the permission to access to external storage, and ask for them if it
     *  does not.
     *  If the app has the permissions, call [getPicture].
     */
    private fun chooseImageFromGallery() {
        if (PermissionChecker.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE) != PermissionChecker.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), PERMISSION_REQUEST_STORAGE)
        else getPicture()
    }

    /**
     *  Checks if the app has the permission to access to camera, and ask for them if it does not.
     *  If the app has the permissions, call [takeNewPicture].
     */
    private fun takePictureFromCamera() {
        if (PermissionChecker.checkSelfPermission(this,
                Manifest.permission.CAMERA) != PermissionChecker.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this,
            arrayOf(Manifest.permission.CAMERA), PERMISSION_REQUEST_CAMERA)
        else takeNewPicture()
    }

    /**
     *  Call the [Intent] that opens the device gallery.
     */
    private fun getPicture() {
        val galleryIntent = Intent(Intent.ACTION_OPEN_DOCUMENT,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(galleryIntent, REQUEST_CODE_GALLERY)
    }

    /**
     *  Call the [Intent] that opens the device camera.
     */
    private fun takeNewPicture() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(cameraIntent, REQUEST_CODE_CAMERA)
    }

    /**
     *  If the user did not filled the required fields, this function will show an error on them an
     *  display an error toast.
     */
    private fun showRequiredFields() {
        if (plainFirstName?.text.isNullOrBlank() && plainLastName?.text.isNullOrBlank())
            plainFirstName?.error = getString(R.string.required_field)
        if (plainPhoneNumber?.text.isNullOrBlank())
            plainPhoneNumber?.error = getString(R.string.required_field)
        Toast.makeText(this, R.string.error_fill_fields, Toast.LENGTH_LONG).show()
    }

    /**
     *  It is better for the code to have a phone number like this : +xxXXXXXXXXX, so we ask the
     *  user to format it properly when saving a contact.
     *  Moreover I am really too lazy to code a formatter by myself.
     */
    private fun showPhoneFormatError() {
        Toast.makeText(this, getString(R.string.phone_number_format_error), Toast.LENGTH_LONG)
            .show()
    }

    /**
     *  Displays a warning to the user when he entered a wrong formatted email.
     */
    private fun badFormattedEmailAddress() {
        Toast.makeText(this, getString(R.string.email_format_error), Toast.LENGTH_LONG)
            .show()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return (true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.menu_black -> { changeHeaderColor(toolbar, COLOR_BLACK) }
        R.id.menu_yellow -> { changeHeaderColor(toolbar, COLOR_YELLOW) }
        R.id.menu_cyan -> { changeHeaderColor(toolbar, COLOR_CYAN) }
        R.id.menu_green -> { changeHeaderColor(toolbar, COLOR_GREEN) }
        R.id.menu_red -> { changeHeaderColor(toolbar, COLOR_RED) }
        else -> { super.onOptionsItemSelected(item) }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == PERMISSION_REQUEST_STORAGE) {
            if (grantResults.contains(PackageManager.PERMISSION_DENIED)) {
                Toast.makeText(this, getString(R.string.storage_permission_needed),
                    Toast.LENGTH_LONG).show()
            } else getPicture()
        } else if (requestCode == PERMISSION_REQUEST_CAMERA) {
            if (grantResults.contains(PackageManager.PERMISSION_DENIED)) {
                Toast.makeText(this, getString(R.string.camera_permission_needed),
                    Toast.LENGTH_LONG).show()
            } else takeNewPicture()
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_CANCELED) return
        if (requestCode == REQUEST_CODE_GALLERY) {
            if (data != null && data.data != null) {
                val contentUri : Uri = data.data!!
                Log.d("NewContactActivity", "contentUri = $contentUri")
                try {
                    buttonAddPicture?.colorFilter = null
                    buttonAddPicture?.setImageURI(contentUri)
                    imageUri = contentUri
                } catch (exception : IOException) {
                    exception.printStackTrace()
                    Toast.makeText(this, getString(R.string.generic_error), Toast.LENGTH_LONG)
                        .show()
                }
            }
        } else if (requestCode == REQUEST_CODE_CAMERA) {
            if (data != null && data.extras != null && data.extras!!.containsKey("data")) {
                val bitmap: Bitmap? = data.extras!!["data"] as Bitmap?
                if (bitmap != null) {
                    buttonAddPicture?.colorFilter = null
                    buttonAddPicture?.setImageBitmap(bitmap)

                    imageUri = ImageHelper().saveImage(this, bitmap)
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    companion object {

        private const val TAG = "NewContactActivity"

        fun getStartIntent(context: Context, contact : Contact?) : Intent {
            val intent = Intent(context, NewContactActivity::class.java)
            intent.putExtra(TAG_CONTACT, contact)
            return intent
        }

        private const val REQUEST_CODE_GALLERY = 6
        private const val REQUEST_CODE_CAMERA = 7

        private const val PERMISSION_REQUEST_STORAGE = 8
        private const val PERMISSION_REQUEST_CAMERA = 9

        private const val TAG_CONTACT = "contact"
    }
}
