package com.mameyer.ft_hangouts.model

import java.io.Serializable

class Contact : Serializable {

    var id = 0L
    var firstName = ""
    var lastName = ""
    var phoneNumber = ""
    var birthDay = ""
    var email = ""
    var company = ""
    var picture = ""

    override fun toString() : String {
        return "CONTACT $id : $firstName $lastName : $phoneNumber | $birthDay | $email | $company"
    }

}